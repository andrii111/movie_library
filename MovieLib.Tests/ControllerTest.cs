﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Generic;

using MovieLib.Controllers;
using MovieLib.Models;

namespace MovieLib.Tests
{
    [TestClass]
    public class ControllerTest
    {
        MovieController controller;

        [TestInitialize]
        public void Setup()
        {
            controller = new MovieController();
        }
        
        [TestMethod]
        public void Check_Main_Index()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsTrue(result.Model.GetType().IsGenericType);
            Assert.AreEqual("",result.ViewName);
        }


        [TestMethod]
        public void Check_Edit_And_Delete()
        {
            ViewResult result = controller.Edit(1) as ViewResult;
            Assert.AreEqual("Movie", result.Model.GetType().Name);
            ViewResult result1 = controller.Delete(1) as ViewResult;
            Assert.AreEqual("Movie", result1.Model.GetType().Name);
        }

        [TestMethod]
        public void Check_Create()
        {
            ViewResult result = controller.Create() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(string.Empty, result.ViewName);
            Movie movie = new Movie();
            movie.Name = "Danylo Halytskyi";
            movie.Country = "Ukraine";
            movie.ReleaseDate = new DateTime(2015,10,12);
            RedirectToRouteResult result1 = controller.Create(movie) as RedirectToRouteResult;
            Assert.AreEqual("Index", result1.RouteValues["Action"]);
            Assert.IsNotNull(result1.ToString());
        }

        [TestMethod]
        public void Check_Find()
        {
            ViewResult result = controller.Find("Bulba") as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(string.Empty, result.ViewName);
            var movies = (IList<Movie>)result.ViewData.Model;
            Assert.IsTrue(movies.Count>0);
        }

        [TestMethod]
        public void Check_SPA_Index()
        {
            SPAController controller = new SPAController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNull(result.Model);
            Assert.AreEqual("", result.ViewName);
        }
    }
}
