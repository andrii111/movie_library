﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

using Moq;
using MovieLib.Repository;
using MovieLib.Models;
using MovieLib.Controllers;
using System.Collections.Generic;

namespace MovieLib.Tests
{
    [TestClass]
    public class MockingTest
    {
        Mock<IMovie> mockRepo;
        MovieController controller;
        Movie newMovie;
        Movie editMovie;
        
        [TestInitialize]
        public void Setup()
        {
            mockRepo = new Mock<IMovie>();
            IList<Movie> list = new List<Movie>();
            list.Add(new Movie(1, "Spartak", "Italy", new DateTime(1965, 05, 10)));
            list.Add(new Movie(2, "Tini Zabutyh predkiv", "Ukraine", new DateTime(1969, 07, 10)));
            list.Add(new Movie(3, "Taras Bulba", "Russia", new DateTime(2010, 10, 05)));
            mockRepo.Setup(x => x.GetMovies()).Returns(list);
            mockRepo.Setup(x => x.GetMovie(2)).Returns(list[1]);
            IList<Movie> list2 = new List<Movie>();
            list2.Add(new Movie(3, "Taras Bulba", "Russia", new DateTime(2010, 10, 05)));
            mockRepo.Setup(x => x.GetMovies("Bulba")).Returns(list2);
            newMovie=new Movie("Zacharova Desna", "Ukraine",new DateTime(1964, 05, 10));
            mockRepo.Setup(x => x.AddMovie(newMovie)).Verifiable();
            editMovie = new Movie(1, "The Italian Brigands", "Italy", new DateTime(1962, 10, 01));
            mockRepo.Setup(x => x.UpdateMovie(editMovie)).Verifiable();
            mockRepo.Setup(x => x.DeleteMovie(It.IsAny<int>())).Verifiable();
            controller = new MovieController(mockRepo.Object);
        }
        
        [TestMethod]
        public void Mock_GetAll()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.AreEqual(3, ((IList<Movie>)result.ViewData.Model).Count);

        }

        [TestMethod]
        public void Mock_Find()
        {
            ViewResult result = controller.Find("Bulba") as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.AreEqual(1, ((IList<Movie>)result.ViewData.Model).Count);

        }

        [TestMethod]
        public void Mock_Create()
        {
            RedirectToRouteResult result = controller.Create(newMovie) as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.IsNotNull(result.ToString());
        }

        [TestMethod]
        public void Mock_Edit()
        {
            RedirectToRouteResult result = controller.Edit(editMovie) as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Mock_Delete()
        {
            RedirectToRouteResult result = controller.Delete(3,new FormCollection()) as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.IsNotNull(result);
        }
    }
}
