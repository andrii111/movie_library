﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using log4net;

using MovieLib.Models;
using MovieLib.Repository;

namespace MovieLib.Controllers
{
    public class UserController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserController));
        UserRepository userRepo = new UserRepository();
        
        //
        // GET: /User/Register

        [HttpGet]
        public ActionResult Register()
        {
            log.Info("Register Form Rendered");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User user)
        {
            bool Status = false;
            string message = "";

            if (ModelState.IsValid)
            {

                var isEmailExist = IsEmailExist(user.Email);
                if (isEmailExist)
                {
                    ModelState.AddModelError("", "Email already exist");
                    return View(user);
                }

                var isUserExist = IsUserExist(user.UserName);
                if (isUserExist)
                {
                    ModelState.AddModelError(string.Empty, "UserName already exist");
                    return View(user);
                }

                var activationCode = Guid.NewGuid();

                user.Password = Utility.Hash(user.Password);
                user.ConfirmPassword = Utility.Hash(user.ConfirmPassword);

                userRepo.AddUser(user);

                    message = "Registration successfully done. Your data:"+ user.UserName+": " + user.Email;
                    log.Info("New user regsitered: "+user.UserName);
                Status = true;
                }
           
            else
            {
                log.Error("Invalid Request");
                message = "Invalid Request";
            }

            ViewBag.Message = message;
            ViewBag.Status = Status;
            return View(user);
        }

        [NonAction]
        public bool IsEmailExist(string email)
        {
                var iee=userRepo.GetUsers().Where(x => x.Email == email).FirstOrDefault();
                return iee != null;
            
        }
        
        [NonAction]
        public bool IsUserExist(string username)
        {
                var iue=userRepo.GetUsers().Where(x => x.UserName == username).FirstOrDefault();
                return iue != null;
            
        }

        [HttpGet]
        public ActionResult Login()
        {
            log.Info("Login Form Rendered");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginInfo login)
        {
            string message = "";
            var u = userRepo.GetUsers().Where(x => x.UserName == login.UserName).FirstOrDefault();
            if (u==null)
            {
                 ViewBag.Message = "Please verify your username first";
                 return View();
            }
            if (string.Compare(Utility.Hash(login.Password), u.Password) == 0)
            {
                int timeout = login.RememberMe ? 43200 : 20; // 43200 min = 1 month (30 Days)
                var ticket = new FormsAuthenticationTicket(login.UserName, login.RememberMe, timeout);
                string encrypted = FormsAuthentication.Encrypt(ticket);
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                cookie.Expires = DateTime.Now.AddMinutes(timeout);
                cookie.HttpOnly = true;
                Response.Cookies.Add(cookie);
                log.Info("Login Succesfull for: "+login.UserName);
                    return RedirectToAction("Index", "Movie");
            }
            else
            {
                log.Error("Invalid credential provided");
                message = "Invalid credential provided";
            }
            ViewBag.Message = message;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            log.Debug("User "+User.Identity.Name+ " Logged out");
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

    }
}
