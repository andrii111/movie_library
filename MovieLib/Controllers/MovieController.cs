﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MovieLib.Repository;
using MovieLib.Models;


namespace MovieLib.Controllers
{
    public class MovieController : Controller
    {
        private IMovie movieRepo;

        public MovieController()
        {
            movieRepo = new MovieRepository();
        }

        public MovieController(IMovie repo)
        {
            movieRepo = repo;
        }
        
        //
        // GET: /Movies/

        [Authorize]
        public ActionResult Index()
        {
            var list = movieRepo.GetMovies().ToList();
            return View(list);
        }

        public ActionResult Find(string term)
        {
            var list = movieRepo.GetMovies(term).ToList();
            return View(list);
        }

        //
        // GET: /Movie/Create
        [Authorize]
        public ActionResult Create()
        {
            return View(new Movie());
        }

        //
        // POST: /Movie/Create

        [HttpPost]
        public ActionResult Create(Movie item)
        {
            try
            {
                var movie = new Movie();
                movie.Name = item.Name;
                movie.Country = item.Country;
                movie.ReleaseDate = item.ReleaseDate;
                movieRepo.AddMovie(movie);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Movie/Edit/5

        public ActionResult Edit(int id)
        {
            var item = movieRepo.GetMovie(id);
            var movie = new Movie();
            movie.Id = id;
            movie.Name = item.Name;
            movie.Country = item.Country;
            movie.ReleaseDate = item.ReleaseDate;
            return View(movie);
        }

        //
        // POST: /Movie/Edit/5

        [HttpPost]
        public ActionResult Edit(Movie movie)
        {
            try
            {
                movieRepo.UpdateMovie(movie);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Movie/Delete/5

        public ActionResult Delete(int id)
        {
            var movie = movieRepo.GetMovie(id);
            return View(movie);
        }

        //
        // POST: /Movie/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                movieRepo.DeleteMovie(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
