﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MovieLib.Models;
using MovieLib.Repository;
using System.Diagnostics;

namespace MovieLib.Controllers
{
    public class SPAController : Controller
    {
        private IMovie movieRepo;

        public SPAController()
        {
            movieRepo = new MovieRepository();
        }
        
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /SPA/

        public ActionResult GetAll()
        {
            var list = movieRepo.GetMovies().ToList();
            return PartialView("_GetAll",list);
        }

        public ActionResult Find(string term)
        {
            var list = movieRepo.GetMovies(term).ToList();
            return PartialView("_Find",list);
        }

        //
        // GET: /SPA/Create

        public ActionResult Create()
        {
            return PartialView("_Create",new Movie());
        }

        //
        // POST: /SPA/Create

        [HttpPost]
        public ActionResult Create(Movie item)
        {
            try
            {
                var movie = new Movie();
                movie.Name = item.Name;
                movie.Country = item.Country;
                movie.ReleaseDate = item.ReleaseDate;
                movieRepo.AddMovie(movie);
                return Content("New Movie Created");
            }
            catch
            {
                return Content("Check errors in New Movie Form ");
            }
        }

        //
        // GET: /SPA/Edit/5

        public PartialViewResult Edit(int id)
        {
            var item = movieRepo.GetMovie(id);
            var movie = new Movie();
            movie.Id = id;
            movie.Name = item.Name;
            movie.Country = item.Country;
            movie.ReleaseDate = item.ReleaseDate;
            return PartialView("_Edit", movie);
        }

        //
        // POST: /SPA/Edit/5

        [HttpPost]
        public ActionResult Edit2(Movie movie)
        {
            try
            {
                
                movieRepo.UpdateMovie(movie);
                return PartialView("_Update");
            }
            catch
            {
                return Content("Not correct values: \n Check Editing fields");
            }
        }

        //
        // GET: /SPA/Delete/5

        public PartialViewResult Delete(int id)
        {
            var movie = movieRepo.GetMovie(id);
            return PartialView("_Delete",movie);
        }

        //
        // POST: /SPA/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                movieRepo.DeleteMovie(id);
                return PartialView("_DeletePost");
            }
            catch
            {
                return Content("There is no such movie");
            }
        }

    }
}
