﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

using MovieLib.Models;

namespace MovieLib.Repository
{
    public class MovieRepository:IMovie
    {
        private MovieContext context;

        public MovieRepository() {
           this.context = new MovieContext();
        }

        public IEnumerable<Movie> GetMovies()
        {
            return context.Movies.ToList();
            
        }

        public IEnumerable<Movie> GetMovies(String text)
        {
            IEnumerable<Movie> movies = context.Movies.Where(e => e.Name.Contains(text) || e.Country.Contains(text)).ToList<Movie>();
            return movies;
        }

        public Movie GetMovie(int id)
        {
            return context.Movies.Find(id);
        }

        public void AddMovie(Movie movie)
        {
            context.Movies.Add(movie);
            context.SaveChanges();

        }

        public void UpdateMovie(Movie item)
        {
            //var movie = context.Movies.Find(item.Id);
            try
            {
                  context.Entry(item).State = EntityState.Modified;
                  context.SaveChanges();
            //    context.Entry(movie).CurrentValues.SetValues(item);
            }
            catch (Exception e) { }
            
            
        }

        public void DeleteMovie(int id)
        {
            Movie m = context.Movies.Find(id);
            try
            {
                context.Movies.Remove(m);
                context.SaveChanges();
            }
            catch (Exception e) { }
            
        }

    }
}