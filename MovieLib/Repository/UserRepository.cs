﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using MovieLib.Models;

namespace MovieLib.Repository
{
    public class UserRepository
    {
        UserContext context = new UserContext();

        public IList<User> GetUsers()
        {
            return context.Users.ToList();
        }

        public User GetUser(int id)
        {
            return context.Users.Find(id);
        }

        public void UpdateUser(User item)
        {
            try
            {
                context.Entry(item).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception e) { }
            
        }

        public void AddUser(User item)
        {
            context.Users.Add(item);
            context.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            User user = context.Users.Find(id);
            try
            {
                context.Users.Remove(user);
                context.SaveChanges();
            }
            catch (Exception e) { }
            
        }

    }
}