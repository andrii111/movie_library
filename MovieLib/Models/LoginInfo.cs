﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace MovieLib.Models
{
    public class LoginInfo
    {
        [Required(ErrorMessage = "Write the Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Write the Password")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}