﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MovieLib.Models
{
    public class UserContext : DbContext
    {
        public UserContext()
            : base("usersInMovieLibrary")
        {
        }
        public DbSet<User> Users { get; set; }
    }
}